package main

import (
	"fmt"
	ics "github.com/arran4/golang-ical"
	"net/http"
	"sort"
	"time"
)

const (
	icsTimeFormat  = "20060102T150405Z"
	icsTimeFormat2 = "20060102"
)

type Record struct {
	Summary string
	Start   time.Time
}

func Get(url string) (*ics.Calendar, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	return ics.ParseCalendar(res.Body)
}

func GetTime(e *ics.VEvent, prop ics.ComponentProperty) (time.Time, error) {
	s := e.GetProperty(prop).Value
	t, err := time.Parse(icsTimeFormat, s)
	if err == nil {
		return t, nil
	}
	return time.Parse(icsTimeFormat2, s)
}

func Calendar(url string) ([]string, error) {
	calendar, err := Get(url)
	if err != nil {
		return nil, err
	}
	t := time.Now()
	var rs []Record
	for _, ev := range calendar.Events() {
		b, err := GetTime(ev, ics.ComponentPropertyDtStart)
		if err != nil {
			return nil, err
		}
		if !t.Before(b) {
			continue
		}
		e, err := GetTime(ev, ics.ComponentPropertyDtEnd)
		if err != nil {
			return nil, err
		}
		s := ev.GetProperty(ics.ComponentPropertySummary).Value
		str := fmt.Sprintf(
			"%s-%s %s",
			b.Format("01/02 15:04"),
			e.Format("15:04"),
			s)
		r := &Record{str, b}
		rs = append(rs, *r)
	}
	sort.Slice(rs, func(i, j int) bool {
		return rs[i].Start.Before(rs[j].Start)
	})
	var ss []string
	for _, r := range rs {
		ss = append(ss, r.Summary)
	}
	return ss, nil
}
