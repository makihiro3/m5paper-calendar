package main

import (
	"bytes"
	"image"
	"image/png"
	"io/ioutil"

	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"
)

func Draw(fontPath string, records []string) ([]byte, error) {
	fnt, err := ioutil.ReadFile(fontPath)
	if err != nil {
		return nil, err
	}
	ft, err := truetype.Parse(fnt)
	if err != nil {
		return nil, err
	}
	opt := truetype.Options{
		Size:              12,
		DPI:               0,
		Hinting:           0,
		GlyphCacheEntries: 0,
		SubPixelsX:        0,
		SubPixelsY:        0,
	}

	imageWidth := 960
	imageHeight := 540
	textTopMargin := 30

	img := image.NewGray(image.Rect(0, 0, imageWidth, imageHeight))
	for i := range img.Pix {
		img.Pix[i] = 255
	}
	face := truetype.NewFace(ft, &opt)

	dr := &font.Drawer{
		Dst:  img,
		Src:  image.Black,
		Face: face,
		Dot:  fixed.Point26_6{},
	}

	for i, v := range records {
		dr.Dot.X = 10
		dr.Dot.Y = fixed.I(textTopMargin * (i + 1))
		dr.DrawString(v)
	}

	buf := bytes.Buffer{}
	err = png.Encode(&buf, img)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
