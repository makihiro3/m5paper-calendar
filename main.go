package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

var (
	url      = flag.String("url", "", "icalendar url")
	output   = flag.String("output", "output.png", "output png filepath")
	fontPath = flag.String("font", "TakaoPGothic.ttf", "font TTF file")
)

func main() {
	flag.VisitAll(func(f *flag.Flag) {
		name := strings.ToUpper(f.Name)
		if val, ok := os.LookupEnv(name); ok {
			if err := f.Value.Set(val); err != nil {
				log.Fatal(err)
			}
		}
	})
	flag.Parse()

	ss, err := Calendar(*url)
	if err != nil {
		log.Fatal(err)
	}
	for _, r := range ss {
		fmt.Println(r)
	}

	png, err := Draw(*fontPath, ss)
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile(*output, png, 0666)
	if err != nil {
		log.Fatal(err)
	}
}
